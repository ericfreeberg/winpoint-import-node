
var fs = require('fs');
var admin = require('firebase-admin');

//WP BOON
/*
var serviceAccount = require('./grassroots-boon-firebase-adminsdk-q12kn-87afa0e000.json');
var staging = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://grassroots-boon.firebaseio.com'
},"staging");
*/

// WP Development
/*
var serviceAccount = require('./winpoint-development-firebase-adminsdk-v23ef-ea21ef8e43.json'); 
var staging = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://winpoint-development.firebaseio.com"
},"staging");
*/

// WP Production

var serviceAccount = require("./winpoint-production-firebase-adminsdk-2l5kx-8babda5552.json");

var staging = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://winpoint-production.firebaseio.com"
});



db = staging.firestore();

// get people data...


var updateLog = {};

var parties = JSON.parse(fs.readFileSync('./data/parties.json', 'utf8'));

var stateAbbreviations = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY","FederalUpperLower"];
var officeHolders = [];
for(var s = 0; s < stateAbbreviations.length; s++){
    officeHolders.push(JSON.parse(fs.readFileSync('./data/' +   stateAbbreviations[s] + '.json', 'utf8')));
}


var sets = [];

// Retrieve District Sets from the DB
var districtSetRef = db.collection('districtSets');
var query = districtSetRef.get()
  .then(snapshot => {
    if (snapshot.empty) {
      console.log('No matching documents.');
      return;
    }  
    
    var size = snapshot.size;
  //  console.log(size);
    var x = 0;
    snapshot.forEach(doc => {
        sets.push(doc.data());
        x++; 
        if(x === size){
           updateSets();
        }    
    });
  })
  .catch(err => {
    console.log('Error getting documents', err);
  });

  




function updateSets(){

    for(var s = 0; s < sets.length; s++){
        var set = sets[s];
        set.name = set.name + " - 2019";
        set.dateEffective = new Date('Jan 1, 2019');

        // meta data. 
        set.releaseId = "1.0.0";
        set.releaseDate = new Date();
        set.releaseLog = [];       // console.log(set.name);

        if(set.chamber == "upper" && set.level == "federal"){
            set.districts =  updateDistrictsFederalUpper(set.districts,set.releaseLog);  
            //console.log(set.districts);
        } else {
            set.districts =  updateDistricts(set.districts,set.releaseLog);  
        }  
        
    }
    addDistrictSets();
}



function updateDistrictsFederalUpper(districts,setlog){

    var _toReturn = [];
    var newIds = [];
    for(var d = 0; d < districts.length; d++){
        // get unique list of ids... Ballot Pedia doesn't have unique ids for the office. 
        if(districts[d].districtId.indexOf("-A") > 0){
            newIds.push(districts[d].districtId.substring(0,districts[d].districtId.indexOf("-A")));
        }
    }
    // SEE IF MEMEBER EXISTS IN DISTRICTS... IF SO UPDATE AND REMOVE MEMBER FROM LIST. 
    var tryCount = 0;
    for(var x = 0; x < newIds.length; x++){
    
    
        //console.log(newIds[x]);
      
        var seatA = false;
        var seatB = false;
      
        var mem0 = false;
        var mem1 = false;
        
        var district = newIds[x];
        // look up current member 2019

        var members =  getMembers(newIds[x]);   
       

        var idA = newIds[x] + "-A";
        var idB = newIds[x] + "-B";


        for(var d = 0; d < districts.length; d++){
            if(districts[d].districtId == idA){
                if(members[0].person.data.id == districts[d].personId){
                    seatA = true;
                    mem0 = true;
                    districts[d].lastUpdated = new Date();
                }
                if(members[1].person.data.id == districts[d].personId){
                    seatA = true;
                    mem1 = true;
                    districts[d].lastUpdated = new Date();
                }
            }
        }

        for(var d = 0; d < districts.length; d++){
            if(districts[d].districtId == idB){
                if(members[0].person.data.id == districts[d].personId){
                    seatB = true;
                    mem0 = true;
                    districts[d].lastUpdated = new Date();
                }
                if(members[1].person.data.id == districts[d].personId){                    
                    seatB = true;
                    mem1 = true;
                    districts[d].lastUpdated = new Date();
                }
            }
        }

      
        if(!seatA && !mem0){
          //  console.log("new member 0 seat A");
            updateDistrictFederalUppper(districts,idA,members[0]);
            seatA = true;
            mem0 = true;
        } 
        if(!seatB && !mem0){
          //  console.log("new member 0 seat B");
            updateDistrictFederalUppper(districts,idB,members[0]);
            seatB = true;
            mem0 = true;
        } 
        if(!seatA && !mem1){
          //  console.log("new member 1 seat A");  
            updateDistrictFederalUppper(districts,idA,members[1]);         
            seatA = true;
            mem1 = true;
        }   
        if(!seatB && !mem1){
           // console.log("new member 1 seat B");
           updateDistrictFederalUppper(districts,idB,members[1]);
            seatB = true;
            mem1 = true;
        }   

    }

    return districts;
   
}

function updateDistrictFederalUppper(districts,districtId,member){
    for(var d = 0; d < districts.length; d++){
        if(districts[d].districtId == districtId){            
            var district = districts[d];
            district.personId = member.person.data.id.toString();
            district.personFirstName = member.person.data.first_name;
            district.personName = member.person.data.name;
            district.personLastName = member.person.data.last_name;
            district.personParty = getParty(member.person.data.party_affiliation);
            district.lastUpdated = new Date();
        }
    }
}



function updateDistricts(districts,setlog){

    var _toReturn = [];
    for(var d = 0; d < districts.length; d++){

        var district = districts[d];
        // look up current member 2019
        var member =  getMember(districts[d].districtId);   
        if(member == null){
        
            // do not make any changes to the district
            // member will reflect prior member. 
    
            setlog.push({
                districtId: districts[d].districtId,
                event: "member not updated",
                discription: "there  was no member to district match from the currentMembers data from ballotpedia." 
            });
           //console.log("no member: " + districts[d].districtId + " - " +  districts[d].districtName);

        } else if(member.person.data.id != district.personId){   
            
            //console.log("member! " + districts[d].districtId + " - " +  districts[d].districtName);
            
            district.personId = member.person.data.id;
            district.personFirstName = member.person.data.first_name;
            district.personName = member.person.data.name;
            district.personLastName = member.person.data.last_name;
            district.personParty = getParty(member.person.data.party_affiliation);
            district.lastUpdated = new Date();
        }
       
        _toReturn.push(district);
    }
   // console.log(_toReturn[0]);
    return _toReturn;
}


function getMember(districtId){

   // console.log(districtId);
    var toReturn = null;
    for(var x = 0; x < officeHolders.length; x++){    
        for(var o = 0; o < officeHolders[x].data.length; o++){
           var _id = officeHolders[x].data[o].office.data.district;    
           if(_id == districtId){
               toReturn = officeHolders[x].data[o];
           }      
        }
    }
    return toReturn;
}




function getMembers(districtId){
    var toReturn = [];
    for(var x = 0; x < officeHolders.length; x++){    
        for(var o = 0; o < officeHolders[x].data.length; o++){
           var _id = officeHolders[x].data[o].office.data.district;    
           if(_id == districtId){
               toReturn.push(officeHolders[x].data[o]);
           }      
        }
    }
    return toReturn;
}



function addDistrictSets(){

    // need to change destination
    // updates the database. 

    if(sets.length <= 0){
        console.log("DONE!");
        process.exit();
    }

    var set = sets.pop();

    db.collection('districtSets').add(set)
    .then(function(docRef){        
        console.log(sets.length + " : " + set.name)
        addDistrictSets();
    })
    .catch(function(error){
        console.log(error);
    });

}





function getParty(id){
    for(var p = 0; p < parties.data.length; p++){

        if(id == parties.data[p].id){
            return parties.data[p].name;
        }
    }
    return null;
}