
const request = require('request');
var fs = require('fs');

var stateAbbreviations = ["AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"];

var url = 'http://api.ballotpedia.org/v3/api/1.1/tables/officeholders/rows?access_token=EDj4EOfNzI3Gf1AqR4KuYEMp15aULROz&filters[status][eq]=Current&filters[office_level][eq]=State&filters[office_branch][eq]=Legislative&limit=1000&filters[district_state][eq]=';




function getData(){

    if(stateAbbreviations.length == 0){
        process.exit();
    }

    var state = stateAbbreviations.pop();

    console.log(state);

    request(url + state, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
     
        var json = JSON.stringify(body);
        fs.writeFile('./data/' + state + '.json', json, 'utf8', function(){
            getData(); 
        });  
      });


}

getData();


