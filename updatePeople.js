// update the people database. 
// update the summary for existing
// create new records for ohters. 

var fs = require('fs');
var admin = require('firebase-admin');
var firebase = require("firebase");
var firestore = require("firebase/firestore");


//WP on BOON
/*
var serviceAccount = require('./grassroots-boon-firebase-adminsdk-q12kn-87afa0e000.json');
var staging = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://grassroots-boon.firebaseio.com'
},"staging");
*/

// WP Development
/*
var serviceAccount = require('./winpoint-development-firebase-adminsdk-v23ef-ea21ef8e43.json'); 
var staging = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://winpoint-development.firebaseio.com"
},"staging");
*/

// WP Production

var serviceAccount = require("./winpoint-production-firebase-adminsdk-2l5kx-8babda5552.json");

var staging = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://winpoint-production.firebaseio.com"
});


db = staging.firestore();


var parties = JSON.parse(fs.readFileSync('./data/parties.json', 'utf8'));


var stateAbbreviations = ["FederalUpperLower","AL","AK","AZ","AR","CA","CO","CT","DE","FL","GA","HI","ID","IL","IN","IA","KS","KY","LA","ME","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VT","VA","WA","WV","WI","WY"];
var officeHolders = [];
for(var s = 0; s < stateAbbreviations.length; s++){
    officeHolders.push(JSON.parse(fs.readFileSync('./data/' +   stateAbbreviations[s] + '.json', 'utf8')));
}

console.log(officeHolders.length);

var people = [];

for(var x = 0; x < officeHolders.length; x++){
    
    for(var o = 0; o < officeHolders[x].data.length; o++){

        var _per = officeHolders[x].data[o].person.data;
  
        var person = {
                id: _per.id.toString(),
                name: _per.name,
                firstName: _per.first_name,
                lastName: _per.last_name,
                partyAffiliation: getParty(_per.party_affiliation),
                ballotpediaJSON: {
                    Summary: _per.summary,
                    LastName: _per.last_name,
                    Name: _per.name,
                    FirstName:_per.first_name
                },
                lastUpdated: new Date()
                
        }  
        people.push(person);
    }
}

console.log("people: " + people.length)


function updateFirestore(){

    if(people.length <= 0){
        // end processs
        console.log("Done");
        process.exit();
    }

    var person = people.pop();    
    var personRef = db.collection('people').doc(person.id);
    
    var getDoc = personRef.get()
      .then(doc => {
        if (!doc.exists) {
            db.collection('people').doc(person.id).set(person).then(function(docRef){
                console.log("added: " + people.length);
                updateFirestore();
            }).catch(function(error){});
        } else {

            // skip update for exisitng people. 
            /*
            db.collection('people').doc(person.id).update(person).then(function(docRef){
                console.log("updated: " + people.length);
                
            }).catch(function(error){});
            */

            updateFirestore();
        }
      })
      .catch(err => {
        console.log('Error getting document', err);
      });

}

updateFirestore();


function getParty(id){
    for(var p = 0; p < parties.data.length; p++){

        if(id == parties.data[p].id){
            return parties.data[p].name;
        }
    }
    return null;
}