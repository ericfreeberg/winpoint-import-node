var fs = require('fs');
var admin = require('firebase-admin');


// WP BOON
/*
var serviceAccount = require('./grassroots-boon-firebase-adminsdk-q12kn-87afa0e000.json');

var staging = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://grassroots-boon.firebaseio.com'
},"staging");
*/


// WP Development
/*
var serviceAccount = require('./winpoint-development-firebase-adminsdk-v23ef-ea21ef8e43.json'); 
var staging = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://winpoint-development.firebaseio.com"
},"staging");
*/

// WP Production

var serviceAccount = require("./winpoint-production-firebase-adminsdk-2l5kx-8babda5552.json");

var staging = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://winpoint-production.firebaseio.com"
});


db = staging.firestore();


// formula id for the one forumla that 
// is based on the party of the incumbant.
var formulaId = "5mxdUmkGJfmRW7hrrNF3"


var recordSets = [];

// Retrieve all recordSets from the DB
var recordSetRef = db.collection('recordSets');
var query = recordSetRef.get()
  .then(snapshot => {
    if (snapshot.empty) {
      console.log('No matching documents.');
      return;
    }  
    var size = snapshot.size;
    var x = 0;
    snapshot.forEach(doc => {
        recordSets.push(doc.data());
        x++; 
        if(x === size){
            updateRecordset();
        }   
    });
  })
  .catch(err => {
    console.log('Error getting documents', err);
  });




  var c = 0;

  function updateRecordset(){

    console.log((recordSets.length - c) +  " : Getting Reference");

    if(c == recordSets.length){
        saveRecordSets();
        return;
    }

    // rename recordset and provide release metadata. 
    recordSets[c].name =  recordSets[c].name + " (2019)";
    recordSets[c].releaseID = "1.0.0";
    recordSets[c].releaseDate = new Date();
    recordSets[c].releaseLog = [];


    // retrieve the districtSet reference and update
    var districtSetRef = db.collection("districtSets");

    if(recordSets[c].level ==  "federal"){

        districtSetRef.where("chamber","==",recordSets[c].chamber).where("level","==",recordSets[c].level).where("releaseId","==","1.0.0").get().then(snap => {
           
            if (snap.empty) {
                console.log('Reference not found');                
                recordSets[c].districtSet = null;

            } else {

                snap.forEach(doc => {
                    recordSets[c].districtSet = doc.ref;
                    if(recordSets[c].formula.id == formulaId){
                        updateScores(doc.data(),recordSets[c]);
                    }    
                });
            }              
            c++;
            updateRecordset();            
        });   

    } else {
    
        districtSetRef.where("chamber","==",recordSets[c].chamber).where("stateAbbreviation","==",recordSets[c].stateAbbreviation).where("level","==",recordSets[c].level).where("releaseId","==","1.0.0").get().then(snap => {
  
            if (snap.empty) {
                console.log('Reference not found');
                recordSets[c].districtSet = null;
           } else {

            snap.forEach(doc => {
                recordSets[c].districtSet = doc.ref;
                if(recordSets[c].formula.id == formulaId){
                    updateScores(doc.data(),recordSets[c]);
                }  
            });
           }
           c++;
           updateRecordset();   

        });    
    }
}


function updateScores(districtSet,recordSet){
    
    var userRecords = recordSet.userRecords; 
    var records = recordSet.records; 
    var districts = districtSet.districts;

    for(var u = 0; u < userRecords.length; u++){
        
        var party = getParty(districts,userRecords[u].districtId);

        if(party == null){
            //skip updates
        } else {
            userRecords[u].variables.Party = party;
            records[u].variables.Party = party;
        
            var score; 
            if(records[u].variables.Party == "R"){
                score =  10*(15-100*((1-records[u].variables.Value)-.45))/15;
            } else if (records[u].variables.Party == "D"){
                score =  10*(15-100*(records[u].variables.Value-.45))/15 
            }
            if(score > 10){ 
                score = 10;
            } 
            if(score < 0){
                score = 0;
            }
            records[u].value = score;
        }
    }
}

function getParty(districts,districtId){
    for(var d = 0; d < districts.length; d++){
        if(districts[d].districtId == districtId){
            if(districts[d].personParty == "Republican Party"){
                return "R";
            } else if (districts[d].personParty == "Democratic Party"){
                return "D";
            }
        }
    }
    return null;
}


function saveRecordSets(){

    
    if(recordSets.length <= 0){
        console.log("DONE!");
        process.exit();
    }

    var set = recordSets.pop();

    if(set.districtSet == null){
        // do not save to DB. 
        saveRecordSets();

    } else {

        db.collection('recordSets').add(set)
        .then(function(docRef){    
            console.log(recordSets.length + " : Saving " +  set.name);  
            saveRecordSets();
        })
        .catch(function(error){
            console.log(error);
        });

    }
}
