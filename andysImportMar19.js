var fs = require('fs');
var Papa = require('papaparse');
var admin = require('firebase-admin');



var serviceAccount = require("./winpoint-production-firebase-adminsdk-2l5kx-8babda5552.json");

var prod = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://winpoint-production.firebaseio.com"
});

db = prod.firestore();


/*
var serviceAccount = require('./grassroots-boon-firebase-adminsdk-q12kn-87afa0e000.json');
var boon = admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://grassroots-boon.firebaseio.com'
},"boon");

db = boon.firestore();
*/


// were are skipping recordset groups at this point. 


// array to hold generated recordset before saving to db. 
let recordSetsToSave = [];


// LIST OF NEW RECORD SET TO IMPORT. 
// FILES ARE LISTED IN 'import-data'
var files = [
    {
        file: '2019-CD-2012-President',
        name: '2019 CD 2012 President',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'lower',
        stateAbbreviation: null,
        level: 'federal'
    },
    {
        file: '2019-CD-2016-President',
        name: '2019 CD 2016 President',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'lower',
        stateAbbreviation: null,
        level: 'federal'
    },
    {
        file: '2019-CD-2018-House',
        name: '2019 CD 2018 House',
        formula: '5mxdUmkGJfmRW7hrrNF3',
        chamber: 'lower',
        stateAbbreviation: null,
        level: 'federal'
    },  
    {
        file: '2019-MN-Hse-2018-Governor',
        name: '2019 MN Hse 2018 Governor',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'lower',
        stateAbbreviation: 'MN',
        level: 'state'
    },
    {
        file: '2019-MN-Hse-2018-Senate-Special',
        name: '2019 MN Hse 2018 Senate Special',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'lower',
        stateAbbreviation: 'MN',
        level: 'state'
    },
    {
        file: '2019-MN-Hse-2018-Senate',
        name: '2019 MN Hse 2018 Senate',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'lower',
        stateAbbreviation: 'MN',
        level: 'state'
    },
    {
        file: '2019-MN-Hse-2018-State-House',
        name: '2019 MN Hse 2018 State House',
        formula: '5mxdUmkGJfmRW7hrrNF3',
        chamber: 'lower',
        stateAbbreviation: 'MN',
        level: 'state'
    },
    {
        file: '2019-MN-Hse-Democratic-Performance',
        name: '2019 MN Hse Democratic Performance',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'lower',
        stateAbbreviation: 'MN',
        level: 'state'
    },
    {
        file: '2019-MN-Sen-2018-Governor',
        name: '2019 MN Sen 2018 Governor',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'upper',
        stateAbbreviation: 'MN',
        level: 'state'
    },
    {
        file: '2019-MN-Sen-2018-Senate-Special',
        name: '2019 MN Sen 2018 Senate Special',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'upper',
        stateAbbreviation: 'MN',
        level: 'state'
    },
    {
        file: '2019-MN-Sen-2018-Senate',
        name: '2019 MN Sen 2018 Senate',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'upper',
        stateAbbreviation: 'MN',
        level: 'state'
    },
    {
        file: '2019-MN-Sen-2018-State-House',
        name: '2019 MN Sen 2018 State House',
        formula: '5mxdUmkGJfmRW7hrrNF3',
        chamber: 'upper',
        stateAbbreviation: 'MN',
        level: 'state'
    },
    {
        file: '2019-MN-Sen-Democratic-Performance',
        name: '2019 MN Sen Democratic Performance',
        formula: 'aCFepEYba7aqjzMPweqw',
        chamber: 'upper',
        stateAbbreviation: 'MN',
        level: 'state'
    }
]


// retrieve formula references
let formulas = [];
function getFormulaRefs(id){
    var docRef = db.collection('/formulas').doc(id);
    docRef.get().then(function(doc) {
        if (doc.exists) {
            formulas[id] = doc.ref;
            console.log("got " + doc.id);
        } else {
        console.log("formula doesn't exist")
        }
    }).catch(function(error) {
        console.log("Error getting document:", error);
    });
}
getFormulaRefs('5mxdUmkGJfmRW7hrrNF3');
getFormulaRefs('aCFepEYba7aqjzMPweqw');



// save genertated record sets to database

function saveRecordSets(){
    if(recordSetsToSave.length <= 0){
        console.log("DONE");
        return;
    } 
    let record = recordSetsToSave.pop();

    db.collection("/recordSets").add(record)
    .then(function(docRef) {     
        console.log("Document written with ID: ", docRef.id);
        saveRecordSets();
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });

}


function processFile(){   
    if(files.length <= 0){
        saveRecordSets();
        return;
    } 
    var fileRef = files.pop();
    getDistrictSet(fileRef);
}

processFile();


function getDistrictSet(fileRef){
    
    // get collection ref.. 
    let distRef; 
    if(fileRef.level == "federal"){
        distRef = db.collection('/districtSets').where("releaseId", "==", "1.0.0").where("chamber", "==", fileRef.chamber).where("level", "==", "federal");
    } else {
        distRef = db.collection('/districtSets').where("releaseId", "==", "1.0.0").where("chamber", "==", fileRef.chamber).where("level", "==", "state").where("stateAbbreviation", "==", fileRef.stateAbbreviation);
    }

    distRef.get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
           getImportFile(fileRef,doc);
        });
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}

function getImportFile(fileRef,districtSet){

    let s = fs.readFileSync('./import-data/' +   fileRef.file + '.csv', 'utf8');
    s = s.substr(1); // note sure why papa parse is adding a space at the beginning of the string.

    Papa.parse(s, {
        header: true,
        complete: function(results) {
           createRecordSet(fileRef,results.data,districtSet);
        }
    });   
}


function createRecordSet(fileRef,file,districtSet){

    var userRecords = getUserRecords(fileRef,file,districtSet);
    var records = getRecords(userRecords,fileRef);
    var d = new Date();

    let recordSet = {
        chamber:fileRef.chamber,
        description: null,
        districtSet: districtSet.ref,
        format: 'percentage',
        formula: formulas[fileRef.formula],
        importReleaseVersion: '1.0.1',
        isNonScoring:false,
        level: fileRef.level,
        maxInputValue: null,
        maxOutputValue:10,
        minInputValue:null,
        minOutputValue:0,
        name: fileRef.name,
        records: records,
        releaseDate: d,
        releaseID: '1.0.1',
        releaseLog: [
            {"description": "Andy's March Update For MN and Federal CD on a subset of recordsets. Defined by releaseID: 1.0.1. These imports are not associated with recordset groups."}
        ],
        stateAbbreviation:fileRef.stateAbbreviation,
        updatedAt: d,
        userRecords: userRecords
    }

    recordSetsToSave.push(recordSet);
    processFile();
}


function getRecords(userRecords,fileRef){

    let records = [];
    for(let u = 0; u < userRecords.length;u++){

            let v  = userRecords[u].variables.Value;
            let record = {
                districtId: userRecords[u].districtId,
                value: getScore(userRecords[u],fileRef.formula),
                variables: {
                    Value: v
                }
            }
            records.push(record);
    }
    return records;
}


function getUserRecords(fileRef,file,districtSet){

    let districts = districtSet.data().districts;
    let userRecords = [];
    for(var i = 0; i < districts.length; i++){
        let userRecord = {
            districtId: districts[i].districtId,
            variables: getVariables(fileRef,file,districts[i])
        }
        userRecords.push(userRecord);
    }
    return userRecords;
}


function getVariables(fileRef,file,district){

    var variables = {};
    for(let f = 0; f < file.length;f++){
        if(file[f].districtID == district.districtId){

            if(file[f].Party != undefined){
                variables = {
                    Party: file[f].Party,
                    Value: file[f].Value
                }
            } else {
                variables = {
                    Value: file[f].Value
                }
            }
        } 
    }
    return variables;
}


function getScore(userRecord,formula){

    let score = 0;

    if(formula == '5mxdUmkGJfmRW7hrrNF3'){

        let party = userRecord.variables.Party;
        let value = userRecord.variables.Value;

        if(party === 'R'){
            score =  10*(15-100*((1-value)-.45))/15;
        } else {
            score =  10*(15-100*(value-.45))/15 
        }

        if(score > 10){ 
            score = 10;
        } 
        if(score < 0){
            score = 0;
        }

    }
    if(formula == 'aCFepEYba7aqjzMPweqw'){
        let value = userRecord.variables.Value;
        score = (22-200*Math.abs((value)-.5))*.5
        if(score > 10){ 
            score = 10;
        } 
        if(score < 0){
            score = 0;
        }
    }
    return score;
}
